import TerserPlugin from 'terser-webpack-plugin';
import {Options} from "webpack";

let opti: Options.Optimization = {
    minimizer: [
        new TerserPlugin({
            terserOptions: {
                compress: {
                    unsafe: true,
                    drop_console: true
                },
                output: {comments: false},
            }
        })
    ]
};

export {opti};